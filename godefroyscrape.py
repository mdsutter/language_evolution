from bs4 import BeautifulSoup
import requests
import re
import pandas as pd

TAG_RE = re.compile(r'<[^>]+>')

def remove_tags(text):
    return TAG_RE.sub('', text)

#num_pages = 48
num_pages = 48
poss_pos = ["prép.", "v. a.", "v. réfl.", "v. n.", "n.", "s. f.", "s. m.", "adv.",
            "p. pas.", "adj.", "adj. f.", "interj.", "fig.", "conj.", "loc. adv."]
diction = {}

for i in range(1, num_pages+1):
    url = "https://fr.wikisource.org/wiki/Lexique_de_l%E2%80%99ancien_fran%C3%A7ais/" + str(i)
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'lxml')
    main_text = soup.ul
    lis = main_text.select("li")
    for item in lis:
        string = str(item)
        # drop the li tags
        no_li = string[4:-5]
        # pull the word out using the <b></b> tags
        word = re.search(r"\<b\>.*?\<\/b\>", no_li).group(0)
        # if the word has alternate content following the first </b> tag, drop it
        if re.search(r"<\/b>\ ", word):
            word = re.search(r".+?(?=<)", word).group(0)
        word = remove_tags(word)
        # check if the word has a "#. " in front of it
        if re.search(r"[1-9]\.\ ", word):
            word = word[3:]
        # check if the word has a '* ' in front of it
        if re.search(r"\*\ ", word):
            word = word[2:]
        
        # pull the rest out to parse, replacing unbreakable spaces with regular spaces first
        no_li = no_li.replace(" ", " ")
        to_parse = re.search(r"(?<=(\<\/b\>[);,.\ ])).*$", no_li).group(0)
        while to_parse[0] == " ":
            to_parse = to_parse[1:]
        # if it has weird extra things, drop them
        if re.search(r"(?<=(\<\/b\>[,.\ ])).*$", to_parse):
            to_parse = to_parse = re.search(r"(?<=(\<\/b\>[,.\ ])).*$", to_parse).group(0)
        # treat ';' cases
        to_parse = to_parse.replace(";", ",")
        # drop tags
        to_parse = remove_tags(to_parse)
        word_pos = []
        # if the part of speech is followed by a comma, as is normal, add it to the list
        if re.search(r"^(.*?),", to_parse):
            pos = re.search(r"^(.*?),\ ?", to_parse).group(0)
            pos = pos[:-2]
            word_pos.append(pos)
            to_parse = re.search(r"(?<=(,)).*$", to_parse).group(0)
            if to_parse[0] == " ":
                to_parse = to_parse[1:]
        # otherwise check against possible parts of speech and append, then drop from parsing string
        else:
            for part in poss_pos: 
                if part in to_parse:
                    word_pos.append(part)
                    to_parse = to_parse[len(part)+1:]
        # if the word is not just a variant of another word, parse the definition(s)
        if not re.search(r"^(.*?)\ V\.", to_parse):
            # take each definition and stick it in a list
            defs = []
            sep_count = to_parse.count("‖")
            for i in range(sep_count+1):
                if to_parse[0] == " ":
                    to_parse = to_parse[1:]
                # check for a new part of speech to add to POS list
                if re.search(r"^(.*?),\ ", to_parse):
                    pos = re.search(r"^(.*?),\ ", to_parse).group(0)
                    pos = pos[:-2]
                    # if it's actually a part of speech in the possible list, add it
                    if pos in poss_pos:
                        word_pos.append(pos)
                        to_parse = re.search(r"(?<=(,\ )).*$", to_parse).group(0)
                # if it's the last definition, just append it
                if i == sep_count:
                    to_parse = remove_tags(to_parse)
                    defs.append(to_parse[:-1])
                # otherwise parse it out, append it, and drop it from the string to be parsed
                else:
                    curr_def = re.search(r"^(.*?)\ ?‖", to_parse).group(0)
                    curr_def = curr_def.replace("‖", "")
                    curr_def = remove_tags(curr_def)
                    defs.append(curr_def)
                    to_parse = re.search(r"(?<=(.‖))\ ?.*$", to_parse).group(0)
            if word in diction.keys():
                temp = diction[word]
                for part in word_pos:
                    temp[0].append(part)
                for item in defs:
                    temp[1].append(item)
                diction[word] = temp
            else:
                data = [word_pos,defs]
                diction[word] = data
# now turn the dictionary into a dataframe
df = pd.DataFrame.from_dict(diction, orient="index", columns=["pos", "def"])
df.index.name = "word"
df.reset_index()
df.to_csv('godefroy.csv', encoding="utf-8")
print("Dictionary built!")